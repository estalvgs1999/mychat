package com.tec.chats.network.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server {

    private ServerSocket serverSocket;
    private final ArrayList<ClientHandler> clientsList;

    public Server() {
        clientsList = new ArrayList<ClientHandler>();
    }

    public void start(int port) throws IOException {
        serverSocket = new ServerSocket(port);
        System.out.println("Server listening \uD83D\uDE80 on port: " + port + " ...");

        //noinspection InfiniteLoopStatement
        while (true) {
            Socket clientSocket = serverSocket.accept();
            ClientHandler clientHandler = new ClientHandler(clientSocket, clientsList);
            clientsList.add(clientHandler);
            clientHandler.start(); // ⚠️start is the runnable method for threads!
            System.out.println("New client connected! ...");
        }
    }

    public void stop() throws IOException {
        serverSocket.close();
    }

    public static void main(String[] args) {
        Server server = new Server();
        try {
            server.start(8080);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
