# myChat

Run this project on your IntelliJ IDLE

### Steps

1. First run the server class:

![imagen.png](./assets/imagen.png)

the result in console will be:

![imagen-1.png](./assets/imagen-1.png)

2. Then run your clients:

![imagen.png](./assets/imagen-2.png)

3. Start chatting!!

![imagen.png](./assets/imagen-3.png)


